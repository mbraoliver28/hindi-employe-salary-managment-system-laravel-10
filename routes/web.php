<?php

use App\Http\Controllers\AuthenticationController;
use Illuminate\Support\Facades\Route;


Route::get('/', [AuthenticationController::class, 'login'])->name('login');
Route::post('handle-login', [AuthenticationController::class, 'handleLogin'])->name('handleLogin');
